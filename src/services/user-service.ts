import { Auth } from "aws-amplify";
import User from "../interfaces/user";

const getUser = async () => {
    const userInfo = await Auth.currentUserInfo();

    if(!userInfo){
        return undefined;
    }

    const user: User = {
        id: userInfo.username,
        firstName: userInfo.attributes.given_name,
        lastName: userInfo.attributes.family_name
    };

    return user;
}

const getAuthToken = async () => {
    return (await Auth.currentSession()).getIdToken().getJwtToken();
}

const signOutUser = async () => {
    return Auth.signOut();
}



const UserService = {
    getUser,
    getAuthToken,
    signOutUser
};

export default UserService;