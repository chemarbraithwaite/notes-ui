import { Auth } from "aws-amplify";
import UserService from "../user-service";

jest.mock('aws-amplify');

describe('User service', () => {
    describe('Get user', () => {
        it('Should return undefined if user is not signed in', async () => {
            (Auth.currentUserInfo as jest.Mock).mockReturnValueOnce(undefined);

            await expect(UserService.getUser()).resolves.toBe(undefined);
        })

        it('Should return user if user is signed in', async () => {

            const id = 'id';
            const firstName = 'Test';
            const lastName = 'User';

            (Auth.currentUserInfo as jest.Mock).mockReturnValueOnce(Promise.resolve({
                username: id,
                attributes: {
                    given_name: firstName,
                    family_name: lastName
                }
            }));

            await expect(UserService.getUser()).resolves.toEqual({
                id,
                firstName,
                lastName
            });
        })
    });

    describe('Get authentication token', () => {
        it('Should return authentication token', async () => {
            const token = 'token';
            (Auth.currentSession as jest.Mock).mockReturnValueOnce(Promise.resolve({
                getIdToken: () => {
                    return {
                        getJwtToken: () => {
                            return token
                        }
                    }
                }
            }));

            await expect(UserService.getAuthToken()).resolves.toBe(token);
        });
    });

    describe('Sign out user', () => {
        it('Should call Auth instance sign out method', async () => {
            await UserService.signOutUser();

            expect(Auth.signOut).toBeCalled();
        });
    });
});