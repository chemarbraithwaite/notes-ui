import { configureStore, ThunkAction, Action, AnyAction, combineReducers, CombinedState } from '@reduxjs/toolkit';
import noteSlice, { NotesState } from '../redux/note-slice/note-slice';
import userSlice, { UserState } from '../redux/user-slice/user-slice';

const combinedReducer = combineReducers(
  {
    user: userSlice,
    notes: noteSlice
  }
);

const rootReducer = (state: CombinedState<{ user: UserState; notes: NotesState; }> | undefined, action: AnyAction) => {
  if (action.type === "user/signout/fulfilled") { // check for action type 
    state = undefined;
  }
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
