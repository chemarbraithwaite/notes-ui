export interface Note{
    id: string
    author: string
    content: string
    title: string
    created: string
    color: string
}