export interface PostNote{
    content: string
    title: string
    color: string
}