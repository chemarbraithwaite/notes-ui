export interface EditNote{
    id: string
    content: string
    title: string
    color: string
}