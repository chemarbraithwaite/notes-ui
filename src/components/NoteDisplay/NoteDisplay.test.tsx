import { configureStore } from "@reduxjs/toolkit";
import { Note } from "../../interfaces/note";
import UserReducer from '../../redux/user-slice/user-slice';
import NotesReducer, { NotesState } from '../../redux/note-slice/note-slice';
import User from "../../interfaces/user";
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import NoteDisplay from "./NoteDisplay";

const title = 'Note Title';
const content = "Note's content";
const date = new Date(2019, 1, 2).toISOString();
const color = "#f44336";

const note: Note = {
    author: 'author',
    color,
    content,
    title,
    created: date,
    id: 'id'
}

const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
}

const preloadedStore = (notesState: NotesState) => {
    const store = configureStore({
        reducer: { user: UserReducer, notes: NotesReducer }, preloadedState: {
            notes: notesState,
            user: {
                error: undefined,
                status: 'success',
                user: user
            }
        }
    });

    return store;
}

const renderNoteDisplay = (failedState = false) => {
    const notesState: NotesState = {
        error: undefined,
        notes: undefined,
        searchText: '',
        status: failedState ? 'failed' : 'success',
        hasMore: true
    };

    const store = preloadedStore(notesState);

    render(
        <Provider store={store}>
            <NoteDisplay note={note} />
        </Provider>
    );

    return store;
}

describe('Note Display', () => {

    it('Note title is displayed', async () => {
        renderNoteDisplay();

        await expect(screen.findByText(title)).resolves.toBeInTheDocument();
    });

    it('Note content is displayed', async () => {
        renderNoteDisplay();

        await expect(screen.findByText(content)).resolves.toBeInTheDocument();
    });

    it('Note date is displayed in local string', async () => {
        renderNoteDisplay();

        const localStringDate = new Date(date).toLocaleString();

        await expect(screen.findByText(localStringDate)).resolves.toBeInTheDocument();
    });
});