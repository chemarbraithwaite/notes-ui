import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ChangeEvent, ChangeEventHandler } from "react";
import { Input, InputGroup, InputGroupAddon, InputGroupText } from "reactstrap";
import { useAppDispatch } from "../../app/hooks";
import { searchNote } from "../../redux/note-slice/note-slice";
import './SearchBar.css';


const SearchBar: React.FunctionComponent = () => {

  const dispatch = useAppDispatch();

  const onSearchTextUpdate: ChangeEventHandler<HTMLInputElement> = (event: ChangeEvent<HTMLInputElement>) => {
    dispatch(searchNote(event.target.value));
  }

  return <>
    <InputGroup id="search__bar">
      <InputGroupAddon addonType="prepend">
        <InputGroupText className="search__prepend"><FontAwesomeIcon icon={faSearch} /></InputGroupText>
      </InputGroupAddon>
      <Input data-cy="search-notes"  id="search__field" placeholder="Search" onChange={onSearchTextUpdate} />
    </InputGroup>
  </>;
}

export default SearchBar;