import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { CirclePicker } from "react-color";
import { toast } from "react-toastify";
import { Button, Form, FormGroup, Input, Label, Modal, ModalBody, Spinner } from "reactstrap";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { saveNote, selectNotesStatus } from "../../redux/note-slice/note-slice";

const AddNoteButton: React.FunctionComponent = () => {

    const dispatch = useAppDispatch();
    const [isSaving, setIsSavingNote] = useState(false);
    const [isAdding, setIsAddingNote] = useState(false);
    const [title, setNoteTitle] = useState<string | undefined>();
    const [content, setNoteContent] = useState<string | undefined>();
    const [color, setNoteColor] = useState<string | undefined>();
    const noteStatus = useAppSelector(selectNotesStatus);

    const onSaveNote: React.MouseEventHandler<HTMLButtonElement> = async (event: React.MouseEvent) => {
        event.stopPropagation();

        if (validateField(color)) {
            toast.warning('Please select a note color');
            return;
        }

        if (validateField(title)) {
            toast.warning('Note must have a title');
            return;
        }

        if (validateField(content)) {
            toast.warning('Note must have content');
            return;
        }

        setIsSavingNote(true);

        try {
            const response = await dispatch(saveNote({
                color: color!,
                content: content!,
                title: title!
            }));

            if (response.meta.requestStatus === "rejected") {
                throw new Error();
            }

            setNoteColor(undefined);
            setNoteTitle(undefined);
            setNoteContent(undefined);
            setIsAddingNote(false);
        } catch (error) {
            toast.error('Error saving note, try again');
        }

        setIsSavingNote(false);
    }

    const validateField = (value: string | undefined) => !value || value?.trim() === '';

    const toggle = () => {
        if (noteStatus === "failed") {
            return;
        }

        setIsAddingNote(!isAdding)
    };

    return <>
        <FontAwesomeIcon data-cy="add-note" onClick={toggle} title={'Add note'} className="add__note d-none d-sm-inline-block" icon={faPlusCircle} size={'2x'} />
        <FontAwesomeIcon data-cy="add-note" onClick={toggle} title={'Add note'} className="add__note d-inline-block d-sm-none" icon={faPlusCircle} size={'1x'} />

        {
            isAdding &&

            <Modal isOpen={true} className={''} backdrop={'static'} keyboard={true}>
                <ModalBody>
                    <Form>
                        <legend>New Note</legend>
                        <FormGroup>
                            <div style={{display: 'inline-block'}} >Color*: &nbsp; <div style={{display: 'inline-block', backgroundColor: `${color ?? 'white'}`, border: "1px solid black", height: "15px", width: "15px"}}></div></div>
                            <CirclePicker onChange={(selectedColor) => setNoteColor(selectedColor.hex)}  styles={{ default: { card: { marginTop: '10px', marginBottom: '10px', paddingTop: '10px', paddingLeft: '10px' , backgroundColor: 'whitesmoke' } } }} circleSize={15} width={"100%"} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="new__note__title">Title*</Label>
                            <Input disabled={isSaving} data-cy='note-title'  type="text" name="title" id="new__note__title" placeholder="Title" onChange={(event) => setNoteTitle(event.target.value)} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="new__note__content">Content*</Label>
                            <Input disabled={isSaving} type="textarea" data-cy='note-content' name="text" id="new__note__content" placeholder="Content" onChange={(event) => setNoteContent(event.target.value)} />
                        </FormGroup>
                        <Button disabled={isSaving} data-cy='save-note' color="primary" onClick={onSaveNote}>{
                            isSaving ? <Spinner title="Saving note" size={"sm"} /> : "Save"
                        }</Button> {' '}
                        <Button disabled={isSaving} color="secondary" onClick={toggle}>Cancel</Button>
                    </Form>
                </ModalBody>
            </Modal>
        }
    </>
}

export default AddNoteButton;