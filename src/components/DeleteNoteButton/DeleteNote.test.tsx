import { act, fireEvent, render, screen, waitFor } from "@testing-library/react";
import User from "../../interfaces/user";
import { toast } from 'react-toastify';
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import DeleteNoteButton from "./DeleteNoteButton";
import { configureStore } from "@reduxjs/toolkit";
import UserReducer from '../../redux/user-slice/user-slice';
import NotesReducer, { NotesState } from '../../redux/note-slice/note-slice';
import { Note } from "../../interfaces/note";

const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
}

const color = '#f44336';
const content = 'Test content';
const title = 'Test Title';
const id = 'id';

const note: Note = {
    color,
    content,
    id,
    title,
    author: user.id,
    created: new Date().toISOString()
}

const mockedDispatch = jest.fn();

jest.mock('react-toastify');

const preloadedStore = (notesState: NotesState) => {
    const store = configureStore({
        reducer: { user: UserReducer, notes: NotesReducer }, preloadedState: {
            notes: notesState,
            user: {
                error: undefined,
                status: 'success',
                user: user
            }
        }
    });

    store.dispatch = mockedDispatch;
    return store;
}

const renderDeleteButton = (failedState = false) => {
    const notesState: NotesState = {
        error: undefined,
        notes: [
            note
        ],
        searchText: '',
        status: failedState ? 'failed' : 'success',
        hasMore: true
    };

    const store = preloadedStore(notesState);

    render(
        <Provider store={store}>
            <DeleteNoteButton note={note} />
        </Provider>
    );

    return store;
}


const openModal = async () => {
    const deleteNoteButton = await screen.findByTitle('Delete note');

    act(() => {
        fireEvent.click(deleteNoteButton);
    });
}

const clickDeleteButton = () => {
    const deleteButton = screen.getByText('Delete');

    act(() => {
        fireEvent.click(deleteButton);
    })
}

describe('Delete Note Button', () => {

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Clicking delete note button opens modal', async () => {
        renderDeleteButton();
        await openModal();


        await expect(screen.findByText('Delete Note')).resolves.toBeInTheDocument();
    });

    it('Clicking cancel closes modal', async () => {
        renderDeleteButton();
        await openModal();

        const cancelButton = screen.getByText('Cancel');

        act(() => {
            fireEvent.click(cancelButton); 
        })

        expect(cancelButton).not.toBeInTheDocument();
    });

    it('Action is not dispatched if cancel button is pressed', async () => {
        renderDeleteButton();
        await openModal();
        
        const cancelButton = screen.getByText('Cancel');

        act(() => {
            fireEvent.click(cancelButton);
        })

        expect(mockedDispatch).toBeCalledTimes(0);
    });
    it('Actions is dispatched when the delete button is clicked', async () => {
        renderDeleteButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'fulfilled'}}));
        await openModal();
        clickDeleteButton();     
        
        await waitFor(mockedDispatch);

        expect(mockedDispatch).toBeCalled();
        
    });

    it('Modal is closed when note is deleted successfully', async () => {
        renderDeleteButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'fulfilled'}}));
        await openModal();
        
        const modalTitle = await screen.findByText('Delete Note');
        clickDeleteButton();      
        
        await waitFor(mockedDispatch);
        expect(modalTitle).not.toBeInTheDocument();
    });

    it('Modal reopens when there is an error deleting a note', async () => {
        renderDeleteButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();
    
        clickDeleteButton();     
        
        await waitFor(mockedDispatch);
        await expect(screen.findByText('Delete Note')).resolves.toBeInTheDocument();
    }, 500000);

    it('Toast is shown when there is an error saving a note', async () => {
        renderDeleteButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();

        clickDeleteButton();     
        
        await waitFor(mockedDispatch);
        expect(toast.error).toBeCalledTimes(1);
        expect(toast.error).toBeCalledWith(`Error deleting '${note.title}', try again`, {
            type: 'error'
        });
    });

    it('Modal is dismissed while deleting', async () => {
        renderDeleteButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();

        const modalTitle = await screen.findByText('Delete Note');
        clickDeleteButton();    
        
        expect(modalTitle).not.toBeInTheDocument();
        
        await waitFor(mockedDispatch);
    });

});
