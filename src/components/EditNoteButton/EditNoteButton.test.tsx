import { act, render, screen, waitFor, waitForElementToBeRemoved } from "@testing-library/react";
import User from "../../interfaces/user";
import { toast } from 'react-toastify';
import '@testing-library/jest-dom';
import { Provider } from "react-redux";
import EditNoteButton from "./EditNoteButton";
import { configureStore } from "@reduxjs/toolkit";
import UserReducer from '../../redux/user-slice/user-slice';
import NotesReducer, { NotesState } from '../../redux/note-slice/note-slice';
import userEvent from '@testing-library/user-event'
import { Note } from "../../interfaces/note";

const user: User = {
    firstName: 'Jane',
    lastName: 'Doe',
    id: 'test'
}

const color = '#f44336';
const content = 'Test content';
const title = 'Test Title';
const author = 'author';
const created = new Date().toISOString();
const id = 'id';

const mockedDispatch = jest.fn();

jest.mock('react-toastify');

const preloadedStore = (notesState: NotesState) => {
    const store = configureStore({
        reducer: { user: UserReducer, notes: NotesReducer }, preloadedState: {
            notes: notesState,
            user: {
                error: undefined,
                status: 'success',
                user: user
            }
        }
    });

    store.dispatch = mockedDispatch;
    return store;
}

const renderEditButton = (failedState = false) => {

    const note: Note = {
        author,
        color,
        content,
        created,
        id,
        title
    };

    const notesState: NotesState = {
        error: undefined,
        notes: [note],
        searchText: '',
        status: failedState ? 'failed' : 'success',
        hasMore: true
    };

    const store = preloadedStore(notesState);

    render(
        <Provider store={store}>
            <EditNoteButton {...note}  />
        </Provider>
    );

    return store;
}


const openModal = async () => {
    const editNoteButtons = await screen.findAllByText('Edit note');
    const editNoteButton = editNoteButtons[0];

    act(() => {
        userEvent.click(editNoteButton);
    });
}


const setNoteTitle = async (editedTitle = 'Edited Title') => {
    const noteTitle = await screen.findByPlaceholderText('Title');
    act(() => {
        userEvent.clear(noteTitle);
        userEvent.type(noteTitle, editedTitle); 
    })
    
}

const setNoteContent = async (editedContent = 'Edited Content') => {
    const noteContent = await screen.findByPlaceholderText('Content');
    act(() => {
        userEvent.clear(noteContent);
        userEvent.type(noteContent, editedContent); 
    })
    
}

const clickSaveButton = () => {
    const saveButton = screen.getByText('Save Changes');

    act(() => {
        userEvent.click(saveButton);
    })
}

describe('Edit Note Button', () => {

    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('Clicking edit note button opens modal', async () => {
        renderEditButton();
        await openModal();


        await expect(screen.findByText('Edit Note')).resolves.toBeInTheDocument();
    });

    it('Clicking cancel closes modal', async () => {
        renderEditButton();
        await openModal();

        const cancelButton = screen.getByText('Cancel');

        act(() => {
            userEvent.click(cancelButton);
        })

        expect(cancelButton).not.toBeInTheDocument();
    });

    it("Note title is set to note's title on open", async () => {
        renderEditButton();
        await openModal();

        await expect(screen.findByDisplayValue(title)).resolves.toBeInTheDocument();
    });

    it("Note content is set to note's content on open", async () => {
        renderEditButton();
        await openModal();

        await expect(screen.findByDisplayValue(content)).resolves.toBeInTheDocument();
    });

    it('Error message is shown if note is saved without title', async () => {
        renderEditButton();
        await openModal();
        await setNoteContent();
        await setNoteTitle('');

        clickSaveButton();

        expect(toast.warning).toBeCalledWith('Note must have a title');
    });

    it('Save note actions is not dispatched if note is saved without title', async () => {
        renderEditButton();
        await openModal();
        await setNoteContent();
        await setNoteTitle('');

        clickSaveButton();

        expect(mockedDispatch).toBeCalledTimes(0);
    });

    it('Error message is shown if note is saved without content', async () => {
        renderEditButton();
        await openModal();
        await setNoteTitle();
        await setNoteContent('');

        clickSaveButton();

        expect(toast.warning).toBeCalledWith('Note must have content');
    });

    it('Save note actions is not dispatched if note is saved without content', async () => {
        renderEditButton();
        await openModal();
        await setNoteTitle();
        await setNoteContent('');

        clickSaveButton();

        expect(mockedDispatch).toBeCalledTimes(0);
    });

    it('Actions is dispatched when the save button is clicked and edit note form is filled', async () => {
        renderEditButton();
        mockedDispatch.mockReturnValue({meta: {requestStatus: 'fulfilled'}});
        await openModal();
        
        const modalTitle = await screen.findByText('Edit Note');

        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitForElementToBeRemoved(modalTitle);

        expect(mockedDispatch).toBeCalledTimes(1);
        
    });

    it('Modal is closed when note is saved successfully', async () => {
        renderEditButton();
        mockedDispatch.mockReturnValue({meta: {requestStatus: 'fulfilled'}});
        await openModal();
        
        const modalTitle = await screen.findByText('Edit Note');

        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitForElementToBeRemoved(modalTitle);
    });

    it('Modal is remains open when there is an error editing a note', async () => {
        renderEditButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();
        
        const modalTitle = await screen.findByText('Edit Note');

        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
        
        await waitFor(mockedDispatch);
        expect(modalTitle).toBeInTheDocument();
    });

    it('Toast is shown when there is an error editing a note', async () => {
        renderEditButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();

        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();     
         
        await waitFor(mockedDispatch);
        expect(toast.error).toBeCalledTimes(1);
        expect(toast.error).toBeCalledWith('Error editing note, try again');
    });

    it('Form fields are disabled while saving', async () => {
        renderEditButton();
        mockedDispatch.mockReturnValue(Promise.resolve({meta: {requestStatus: 'rejected'}}));
        await openModal();

        await setNoteTitle();
        await setNoteContent();
        clickSaveButton();    
        
        const titleInput = screen.getByPlaceholderText('Title');
        const contentInput = screen.getByPlaceholderText('Content');

        expect(titleInput).toBeDisabled();
        expect(contentInput).toBeDisabled();
        
        await waitFor(mockedDispatch);
    });

});