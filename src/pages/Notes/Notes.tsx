import { useEffect } from 'react';
import { Button, Spinner } from 'reactstrap';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import MenuBar from '../../components/MenuBar/MenuBar';
import NoteDisplay from '../../components/NoteDisplay/NoteDisplay';
import SearchBar from '../../components/SearchBar/SearchBar';
import { getNotes, selectIsEndOfNotes, selectNotes, selectNotesStatus, selectSearchText } from '../../redux/note-slice/note-slice';
import { selectUserState } from '../../redux/user-slice/user-slice';
import errorLoadigNotesImage from '../../assets/images/error.png';
import notNotesImage from '../../assets/images/sticky-notes.png';
import noSearchResultImage from '../../assets/images/search.png';
import './Notes.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import InfiniteScroll from 'react-infinite-scroll-component';
import { toast } from 'react-toastify';
import { AppConstants } from '../../constants';


const Notes: React.FunctionComponent = () => {

    const notesStatus = useAppSelector(selectNotesStatus);
    const notes = useAppSelector(selectNotes);
    const userState = useAppSelector(selectUserState);
    const searchText = useAppSelector(selectSearchText);
    const hasMore = useAppSelector(selectIsEndOfNotes);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (notesStatus === "idle") {
            dispatch(getNotes())
        }
    }, [notesStatus, dispatch]);


    const getNotesDisplay = () => {

        if (!notes) {

            if (notesStatus === "failed") {
                return <>
                    <div className="image__display">
                        <div>
                            <img className="col-8 col-md-6 col-lg-3" src={errorLoadigNotesImage} alt="Error loading notes" />
                            <p>Failed to load notes, try again</p>
                        </div>

                        <Button onClick={() => dispatch(getNotes())} title="Reload notes"><FontAwesomeIcon icon={faRedo} /></Button>
                    </div>
                </>
            }

            return <>
                <div className="loading">
                    <Spinner /> &nbsp;
                    Loading notes...
                </div>
            </>;
        }

        return <>
            <SearchBar />
            <div>
                {getNotesList()}
            </div>
        </>
    }

    const getNotesList = () => {

        if (notes?.length === 0) {
            return <div className="notes__list">
                <div className="image__display">
                    <div >
                        <img className="col-8 col-md-6 col-lg-3" src={notNotesImage} alt="No notes" />
                        <p>Nothing to see here...</p>
                        <p>Click the + on the menu bar to start adding notes</p>
                    </div>
                </div>
            </div>
        }
        
        const filteredNotes = getFilteredNotes();

        if (filteredNotes.length === 0) {
            return <div className="notes__list">
                <div className="image__display">
                    <div >
                        <img className="col-8 col-md-6 col-lg-3" src={noSearchResultImage} alt="No notes matches search" />
                        <p>No matches...</p>
                    </div>
                </div>
            </div>
        }

        const itemsPerRow = Math.floor(window.innerWidth / AppConstants.noteDisplayWidth);
        const approximatePageRows = Math.ceil(( window.innerHeight * AppConstants.notesDisplayContainerHeight * 0.01 ) / AppConstants.noteDisplayHeight);
        const itemsToFillPage = itemsPerRow * (approximatePageRows - 1) + 1;
        const pageFiiled = filteredNotes.length >= itemsToFillPage;
        let paddingHeight = 0;

        if(!pageFiiled){
            const missingRowCount = approximatePageRows - Math.floor(filteredNotes.length / itemsPerRow);
            paddingHeight = AppConstants.noteDisplayHeight * missingRowCount;
        }

        return <InfiniteScroll
            dataLength={filteredNotes.length}
            hasMore={hasMore}
            loader={<><Spinner /> &nbsp;</>}
            endMessage={<></>}
            next={loadMoreNotes}
            className="notes__list"
        >
            {filteredNotes.map(note => {
                return <NoteDisplay key={note.id} note={note} />
            })}
            <div style={{height: `${paddingHeight}px`}} >

            </div>
        </InfiniteScroll>

    }

    const loadMoreNotes = async () => {
        const result = await dispatch(getNotes(notes![notes!.length - 1].id));

        if (result.meta.requestStatus === 'rejected') {
            toast.error('Failed to load more notes', { type: 'error' });
        }
    }

    const getFilteredNotes = () => {
        return notes!.filter(note => {
            return note.content.toLowerCase().includes(searchText.toLowerCase()) || note.title.toLowerCase().includes(searchText.toLowerCase());
        });
    };


    return <>
        <div style={{height: `${AppConstants.notesDisplayContainerHeight}vh`}} id="notes__display">
            {getNotesDisplay()}
        </div>
        <MenuBar user={userState.user!} />
    </>
}

export default Notes;