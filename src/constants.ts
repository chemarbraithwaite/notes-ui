export class AppConstants {
    static noteDisplayHeight = 250;
    static noteDisplayWidth = 250;
    static noteTitleWidth = 200;
    static notesDisplayContainerHeight = 93.5;
}