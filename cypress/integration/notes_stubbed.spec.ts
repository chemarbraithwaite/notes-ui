import { config } from '../appconfig';

const login = () => {
    cy.visit('http://localhost:3000');
    cy.waitFor('Enter your email address');
    cy.get('[data-test="sign-in-email-input"]').type(config.testUserEmail);

    cy.get('[data-test="sign-in-password-input"]').focus().click({force: true}).type(config.testUserPassword);
    cy.get('[type="submit"]').last().click();
}


describe('Notes (Stubbed Responses)', () => {
    const serverUrlMatcher = /.*\/users\/.*\/notes.*/g;
    const firstPageUrlMatcher = /.*\/users\/.*\/notes/g;
    const nextPageUrlMatcher = /.*\/users\/.*\/notes\/\?id=.*/g;
    const noteTitle = 'Cypress Test';
    const noteContent = 'Sample note content';
    const note = {
        "content": noteContent,
        "created": "2021-08-21T18:21:52.076Z",
        "id": "2021-08-21T18:21:52.076Zecfbd2ac-698a-49f2-9630-a7d8263fc159",
        "color": "#e91e63",
        "title": noteTitle,
        "author": "7c7b75a6-e144-40da-8d28-9ffc84bd3d5d"
    };

    beforeEach(() => {
        login();
    });

    it('Deleting note', () => {
        cy.intercept('GET', firstPageUrlMatcher, {
            statusCode: 200,
            body: {
                notes: [note]
            }
        });

        cy.intercept('GET', nextPageUrlMatcher, {
            statusCode: 200,
            body: {
                notes: []
            }
        })

        cy.intercept('DELETE', serverUrlMatcher, {
            statusCode: 200,
            body: true
        });

        cy.contains(noteTitle);
        cy.get('[data-cy="delete-note"]').click();
        cy.get('[data-cy="confirm-delete"]').click();
        cy.contains('Nothing to see here...');
    });

    it('Adding notes', () => {
        cy.intercept('POST', serverUrlMatcher, {
            statusCode: 200,
            body: note
        });

        cy.intercept('GET', serverUrlMatcher, {
            statusCode: 200,
            body: { notes: [] }
        });

        cy.contains('Nothing to see here...');

        cy.get('[data-cy="add-note"]').first().click();

        //Checking toast messages
        cy.get('[data-cy="save-note"]').click();
        cy.contains('Please select a note color');

        cy.get('[title="#e91e63"]').click();

        cy.get('[data-cy="save-note"]').click();
        cy.contains('Note must have a title');

        cy.get('[data-cy="note-title"]').type(noteTitle);

        cy.get('[data-cy="save-note"]').click();
        cy.contains('Note must have content');

        cy.get('[data-cy="note-content"]').type(noteContent);

        //Saving note
        cy.get('[data-cy="save-note"]').click();

        cy.contains(noteTitle);

    });

    it('Editing note', () => {
        const editedTitle = 'Edited';
        const editedContent = 'Edited noted content';

        cy.intercept('PATCH', serverUrlMatcher, {
            statusCode: 200,
            body: true
        });

        cy.intercept('GET', firstPageUrlMatcher, {
            statusCode: 200,
            body: { notes: [note] }
        });

        cy.intercept('GET', nextPageUrlMatcher, {
            statusCode: 200,
            body: {
                notes: []
            }
        })



        cy.get('[data-cy="edit-note"]').first().click();
        cy.get('[data-cy="note-title"]').clear().type(editedTitle);
        cy.get('[data-cy="note-content"]').clear().type(editedContent);

        cy.get('[data-cy="save-note"]').click();

        cy.contains(editedTitle);
    });

    it('Search note', () => {
        cy.intercept('GET', firstPageUrlMatcher, {
            statusCode: 200,
            body: { notes: [note] }
        });

        cy.intercept('GET', nextPageUrlMatcher, {
            statusCode: 200,
            body: {
                notes: []
            }
        });

        cy.contains(note.title);
        cy.get('[data-cy="search-notes"]').type('Any non matching text');
        cy.contains('No matches...')
        cy.get('[data-cy="search-notes"]').clear();
        cy.contains(note.title);

    });

    it('Scrolling loads more notes', { scrollBehavior: false }, () => {
        const moreNote = {
            ...note,
            id: 'note note id',
            title: 'More note title',
            content: 'More note content'
        }


        cy.intercept('GET', firstPageUrlMatcher, {
            statusCode: 200,
            body: { notes: [note] }
        });

        cy.intercept('GET', nextPageUrlMatcher, {
            statusCode: 200,
            body: {
                notes: [moreNote]
            }
        });

        cy.get('[data-cy="note-display-title"]').should('have.length', 1);
        cy.scrollTo('bottom');
        cy.contains(moreNote.title);
        cy.get('[data-cy="note-display-title"]').should('have.length', 2);
    });

});
