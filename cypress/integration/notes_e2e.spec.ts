import { config } from '../appconfig';

const login = () => {
    cy.visit('http://localhost:3000');
    cy.waitFor('Enter your email address');
    cy.get('[data-test="sign-in-email-input"]').type(config.testUserEmail);

    cy.get('[data-test="sign-in-password-input"]').focus().click({force: true}).type(config.testUserPassword);
    cy.get('[type="submit"]').last().click();
}


describe('Notes (End-To-End)', () => {

    beforeEach(() => {
        login();
    });


    it('Adding, editing and delete note', () => {
        const epoch = new Date().getTime();
        const noteTitle = `Title-${epoch}`;
        const noteContent = `Content-${epoch}`;
        const editedTitle = `Edited-${epoch}`;
        const editedContent = `Edited content-${epoch}`;


        cy.get('[data-cy="add-note"]').first().click();
        cy.get('[title="#e91e63"]').click();
        cy.get('[data-cy="note-title"]').type(noteTitle);
        cy.get('[data-cy="note-content"]').type(noteContent);
        cy.get('[data-cy="save-note"]').click();
        cy.get('[data-cy="save-note"]').should('not.exist');

        cy.reload();

        cy.contains(noteTitle);

        cy.get('[data-cy="edit-note"]').first().click();
        cy.get('[data-cy="note-title"]').clear().type(editedTitle);
        cy.get('[data-cy="note-content"]').clear().type(editedContent);

        cy.get('[data-cy="save-note"]').click();
        cy.get('[data-cy="save-note"]').should('not.exist');

        cy.reload();

        cy.contains(editedTitle);

        cy.get('[data-cy="delete-note"]').first().click();
        cy.get('[data-cy="confirm-delete"]').click();
        cy.get('[data-cy="confirm-delete"]').should('not.exist');
        cy.contains(editedTitle).should('not.exist')

        cy.reload();
 
        cy.contains(editedTitle).should('not.exist');
    });

});