![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/chemarbraithwaite/notes-ui?branch=master) ![Gitlab code coverage](https://img.shields.io/gitlab/coverage/chemarbraithwaite/notes-ui/master)

This is the front-end for a simple, responsive web application that allows users to take small notes. The application was developed using [React](https://reactjs.org/) and [TypeScript](https://www.typescriptlang.org/).

[Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) was used to manage state, and [Amazon Cognito](https://aws.amazon.com/cognito/) and [AWS Amplify](https://docs.amplify.aws/) to manage auth.

## Configurations

The follow needs to be configured to run the application in your own environment

### Amplify

AWS Cognito is used to manage auth, through AWS Amplify. The Congito user pool is deployed as a part of the [back-end](https://gitlab.com/chemarbraithwaite/notes-server) stack and then imported into the Amplify project. 

1. Follow the [instructions](https://gitlab.com/chemarbraithwaite/notes-server) for deploying the back-end stack to your AWS environment. 
2. Install AWS Amplify in this project by following the [documentation here](https://docs.amplify.aws/start/getting-started/installation/q/integration/react/#option-2-follow-the-instructions).
3. Run `amplify import auth` in the root of your project folder and select the user pool that was deployed earlier.

*<b> Note: Make sure the back-end is deployed to the same environment you are using to configure Amplify.</b>*

### API

You will need to set the URL for the API that was deployed in the back-end stack. 

1. Get the URL from the AWS console or otherwise
2. Create a .env file in the root directory
3. Paste `REACT_APP_API_BASE_URL='URL'`, replacing *URL* with the URL for the API

### Testing

To run the E2E tests, create a file named *appconfig.ts* in the root of the `cypress` folder and paste the following: 
```typescript
    export const config = {
        testUserEmail: 'username',
        testUserPassword: 'password'
    }
```

Replace `username` and `password` with the actual username and password of your testing account.

# Screenshots

![Alt No notes](/screenshots/empty.PNG?raw=true)

### Adding notes 

![Alt Add note](/screenshots/add_note.PNG?raw=true)

### Notes

![Alt Notes](/screenshots/notes.PNG?raw=true)
